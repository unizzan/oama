<div align="center">
![oama-logo](./oama-logo.jpg)

OAMA is a web application for instant and real time communication with no required login to chat with other users.
</div>

# Documentation

Documentation available [here](https://gitlab.com/unizzan/oama/-/wikis/home).